import {Component} from 'react'
import './Border.css';
import Color from '../Color/Color.js'
import FontWeight from '../FontWeight/FontWeight.js'

class Border extends Component {
    render() {
        return (
            <div className='Border'>
                <h1>{this.props.title}</h1>
                <div>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export default Border


{/* <FontWeight fontWeight={this.props.fontWeight}>
<Color color='blue'>{this.props.children}</Color> 
</FontWeight> */}